<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

use App\Models\Postagem;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class PostagemTest extends TestCase
{
    use DatabaseTransactions;

    protected $usuario;
    protected $postagem;

    protected function setUp() : void
    {
        parent::setUp();
        // Cria o usuário e a postagem para testes no sistema.

        $this->usuario = User::factory()->create(); // Cria um usuário baseado no arquivo database/factories/PostagemFactory.php

        $this->postagem = Postagem::factory()->create([
            'user_id' => $this->usuario->id
        ]);

        $this->be($this->usuario); // Faz o login do usuário dentro do ambiente de testes do laravel
    }
    
    public function test_admin_postagens_index(){
        //
        // TESTA SE A PAGINA DE POSTAGENS ESTÁ ACESSIVEL E SE POSSUI A INFORMAÇÃO postagens
        //
        $this->get('/admin/postagens')->assertStatus(200); // VERIFICA SE A PÁGINA ESTÁ ACESSIVEL (CÓDIGO 200)
        $this->get('/admin/postagens')
            ->assertViewHas('postagens');// Verfica se existe na página a variavel postagens (Onde estão nossos dados)
        
        $this->assertAuthenticated();
    }

    public function test_admin_postagens_create(){
        //
        // TESTA SE A PAGINA DE POSTAGENS ESTÁ ACESSIVEL E SE POSSUI A INFORMAÇÃO postagens
        //
        $this->get('/admin/postagens/novo')->assertStatus(200);
        $this->get('/admin/postagens/novo')->assertSee('Nova Postagem'); // Verificamos se existe a palavra 'nova postagem'
    }

    public function test_admin_postagens_edit(){
        //
        // TESTA SE A PAGINA DE EDIÇÃO DE POSTAGENS ESTÁ ATIVA
        //

        $this->get('/admin/postagens/editar/' . $this->postagem->id)->assertStatus(200);
        $this->get('/admin/postagens/editar/' . $this->postagem->id)->assertViewHas('postagem');
        $this->get('/admin/postagens/editar/' . $this->postagem->id)->assertSee($this->postagem->titulo);
    }

    public function test_admin_postagens_show(){
        $this->get('/admin/postagens/' . $this->postagem->id)->assertStatus(200);
        $this->get('/admin/postagens/' . $this->postagem->id)->assertViewHas('postagem');
        $this->get('/admin/postagens/' . $this->postagem->id)->assertSee($this->postagem->titulo);
        $this->get('/admin/postagens/' . $this->postagem->id)->assertSee($this->postagem->subtitulo);
        $this->get('/admin/postagens/' . $this->postagem->id)->assertSee($this->postagem->texto);
        $this->get('/admin/postagens/' . $this->postagem->id)->assertSee("Visualizações: {$this->postagem->vizualizacoes}");
    }

    public function test_admin_can_create_postagem(){
        $dados = 
        [
            'titulo'    => 'O Grande Teste',
            'subtitulo' => 'Um teste nunca testa demais',
            'texto'     => 'Este texto é pequeno porque não precisa ser grande'
        ];

        $this->actingAs($this->usuario)
            ->post(route('postagens.store'), $dados)
            ->assertStatus(302); // Se a página foi redirecionada
            
        $this->assertDatabaseHas('postagens', $dados); // Verifica se existem os dados no banco de dados
    }
}
