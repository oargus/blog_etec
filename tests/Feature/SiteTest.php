<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Postagem;
use App\Models\User;
use Faker\Factory;

use Tests\TestCase;

class SiteTest extends TestCase
{

    protected $usuario;
    protected $postagem;

    protected function setUp(): void
    {
        parent::setUp();

        // Cria o usuário e a postagem para testes no sistema.
        $this->usuario = User::factory()->create(); // Cria um usuário baseado no arquivo database/factories/PostagemFactory.php

        $this->postagem = Postagem::factory()->create([
            'user_id' => $this->usuario->id
        ]);

    }

    public function test_user_can_access_home(){
        //
        // VERIFICA SE O USUÁRIO PODE ACESSAR A TELA INICIAL
        //

        $this->get("/")->assertStatus(200)
             ->assertViewHas('postagens');
    }

    public function test_postagem_views_is_incrementing(){
        //
        // VERIFICA SE DEPOIS DE ACESSAR A POSTAGEM A QUANTIDADE DE VIEWS AUMENTA
        //

        $visualizacoes = $this->postagem->vizualizacoes;

        $this->get("/postagem/{$this->postagem->id}")->assertStatus(200);

        $this->postagem->refresh();

        $this->assertGreaterThan($visualizacoes, $this->postagem->vizualizacoes);
    }

    

}
