<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postagem extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'postagens'; // INFORMA O NOME DA TABELA QUE O MODELO VAI USAR
    protected $fillable = ['titulo', 'subtitulo', 'texto', 'imagem', 'vizualizacoes', 'user_id']; // INFORMAR QUAIS OS CAMPOS QUE O USUARIO PODE PREENCHER NO FORMULÁRIO

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function comentarios(){
        return $this->morphMany(Comentario::class, 'commentable');
    }

    public function incrementarVisualizacao(){
        $this->vizualizacoes += 1;
        $this->save();
    }
}