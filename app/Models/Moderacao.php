<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Moderacao extends Model
    {
        use HasFactory;
        use SoftDeletes;

        protected $table = "moderacoes";
        protected $fillable = ['estado', 'parecer', 'postagem_id', 'user_id'];

        public function user(){
            return $this->belongsTo(User::class);
        }

        public function postagem(){
            return $this->belongsTo(Postagem::class);
        }
    }
