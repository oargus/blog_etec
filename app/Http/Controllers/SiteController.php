<?php

namespace App\Http\Controllers;

use App\Models\Postagem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    function __construct(){
        $this->tamanhoPaginacao = 5;
    }

    public function index(){
        $postagens = Postagem::latest()->paginate($this->tamanhoPaginacao);

        $postagensFamosas = Postagem::orderBy('vizualizacoes', 'desc')->limit(5)->get();

        return view('site.home', compact('postagens', 'postagensFamosas'));
    }

    public function pesquisar(Request $request){
        $filters = $request->except('_token');

        $postagens = Postagem::where("titulo", 'LIKE', "%{$request->pesquisa}%")
                            ->orWhere('subtitulo', 'LIKE', "%{$request->pesquisa}%")
                            ->orWhere('texto', 'LIKE', "%{$request->pesquisa}%")
                            ->paginate($this->tamanhoPaginacao);

        return view('site.home', compact('postagens', 'filters'));
    }

    public function postagem($id){
        $postagem = Postagem::findOrFail($id);

        $postagem->incrementarVisualizacao();

        return view('site.postagem', compact('postagem'));
    }

    public function cadastroUsuario(){
        $etecs = DB::table('etecs')->orderBy('codigo', 'ASC')->get();
        
        return view('site.cadastro_usuario', compact('etecs'));
    }

    public function login(){
        return view('site.login');
    }

}
