<?php

namespace App\Http\Controllers;

use App\Models\Postagem;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUpdatePostagens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostagemController extends Controller
{
    public function index(){
        $postagens = Postagem::where('user_id', Auth::user()->id)->get();

        return view('admin.postagens.index', compact('postagens'));
    }

    public function create(){
        return view('admin.postagens.create');
    }

    public function store(StoreUpdatePostagens $request){

        $dados = $request->all();

        if($request->imagem && $request->imagem->isValid()){
            $nome_imagem = Str::of($request->titulo . date('dmYHis'))->slug('-') . '.' . $request->imagem->getClientOriginalExtension();

            $imagem = $request->imagem->storeAs('postagem', $nome_imagem);
            $dados['imagem'] = $imagem;
        }

        $dados['user_id'] = Auth::user()->id;

        $postagem = Postagem::create($dados);

        return redirect()
                    ->route('postagens.show', $postagem->id)
                    ->with('mensagem', 'Postagem criada com sucesso');
    }

    public function update(StoreUpdatePostagens $request, $id){
        if(!$postagem = Postagem::find($id))
            return redirect()->back();


        if ($postagem->user <> Auth::user()) {
            return redirect()->route('postagens.index')
            ->with("mensagem", "Você não tem acesso a esta postagem");
        }

        $dados = $request->all();

        if ($request->imagem && $request->imagem->isValid()) {
            if(Storage::exists($postagem->imagem)) // VERIFICA SE EXISTE IMAGEM NA POSTAGEM
                Storage::delete($postagem->imagem); // SE EXISTE APAGA

            $nome_imagem = Str::of($request->titulo . date('dmYHis'))->slug('-') . '.' . $request->imagem->getClientOriginalExtension();

            $imagem = $request->imagem->storeAs('postagem', $nome_imagem);
            $dados['imagem'] = $imagem;
        }

        $postagem->update($dados);

        return redirect()
            ->route('postagens.edit', $postagem->id)
            ->with('mensagem', "Postagem editada com sucesso!");
    }

    public function show($id){
        $postagem = Postagem::find($id);
        
        if(!$postagem)
            return redirect()->route('postagens.index');

        if($postagem->user->id <> Auth::user()->id){
            return redirect()->route('postagens.index')
                            ->with("mensagem", "Você não tem acesso a esta postagem");
        }

        return view('admin.postagens.show', compact('postagem'));
    }

    public function edit($id){
        $postagem = Postagem::find($id);

        if(!$postagem)
            return redirect()->back(); // SE NÃO ENCONTRAR POSTAGEM VOLTA PRA DE ONDE VEIO

        if ($postagem->user->id <> Auth::user()->id) {
            return redirect()->route('postagens.index')
            ->with("mensagem", "Você não tem acesso a esta postagem");
        }

        return view('admin.postagens.edit', compact('postagem'));
    }

    public function destroy($id){
        $postagem = Postagem::find($id);

        if(!$postagem) // SE NÃO ENCONTRAR POSTAGEM
            return redirect()->route('postagens.index');

        if ($postagem->user <> Auth::user()) {
            return redirect()->route('postagens.index')
            ->with("mensagem", "Você não tem acesso a esta postagem");
        }

        if (Storage::exists($postagem->imagem)) // VERIFICA SE EXISTE IMAGEM NA POSTAGEM
            Storage::delete($postagem->imagem); // SE EXISTE APAGA

        $postagem->delete();

        return redirect()
            ->route('postagens.index')
            ->with('mensagem', 'Postagem deletada com sucesso');
    }
}
