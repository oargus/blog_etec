<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModeracoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moderacoes', function (Blueprint $table) {
            $table->id();
            $table->string('estado', 20);
            $table->longText('parecer');

            $table->foreignId('postagem_id')
            ->references('id')->on('postagens')
            ->constrained();

            $table->foreignId('user_id')
            ->references('id')->on('users')
            ->constrained();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moderacoes');
    }
}
