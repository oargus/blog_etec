<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateComentariosPolimorfismoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comentarios', function(Blueprint $table){
            $table->integer('commentable_id')->unsigned();
            $table->string('commentable_type');

            $table->dropForeign('comentarios_postagem_id_foreign');
            $table->dropColumn('postagem_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comentarios', function(Blueprint $table){
            $table->dropColumn('commentable_id');
            $table->dropColumn('commentable_type');

            $table->integer('postagem_id')->unsigned();

            $table->foreignId('postagem_id')
                ->references('id')->on('postagens')
                ->constrained();
        });
    }
}
