<?php

namespace Database\Factories;

use App\Models\Postagem;
use Illuminate\Database\Eloquent\Factories\Factory;


class PostagemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Postagem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->sentence(),
            'subtitulo' => $this->faker->sentence(),
            'texto' => $this->faker->text(500),
            'vizualizacoes' => $this->faker->numberBetween(0, 999999),
            'user_id' => 1
        ];

        
    }
}
