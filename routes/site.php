<?php
    use Illuminate\Support\Facades\Route;

    use App\Http\Controllers\{
        SiteController
    };

    use App\Http\Controllers\Auth\{
        RegisteredUserController,
        AuthenticatedSessionController
    };


    // ROTAS DO SITE
    Route::group(['middleware' => 'auth'], function(){
        Route::get('/', [SiteController::class, 'index'])->name('site.home');
        Route::get('/postagem/{id}', [SiteController::class, 'postagem'])->name('site.postagem');

        Route::any('/procurar', [SiteController::class, 'pesquisar'])->name('postagem.search');
    });

    // ROTAS DO SITE
    Route::group(['middleware' => 'guest'], function(){
        Route::get('/cadastrar', [SiteController::class, 'cadastroUsuario'])->name('site.usuarios.create');
        Route::post('/cadastrar', [RegisteredUserController::class, 'store'])->name('site.usuarios.store');
    
        Route::get('/entrar', [SiteController::class, 'login'])->name('login');
        Route::post('/entrar', [AuthenticatedSessionController::class, 'store'])->name('entrar');
    });
    
    Route::group(['middleware' => 'auth'], function () {   
        Route::any('/sair', [AuthenticatedSessionController::class, 'destroy'])->name('sair');
    });
?>                  