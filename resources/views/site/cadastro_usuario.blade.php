@extends('site.index')

@section('titulo', 'Cadastro de usuário')

@section('conteudo')
    <h1>Novo usuário | EtecNews</h1>

    @if ( $errors->any() )
        <strong>Opa!</strong> As informações inseridas não são validas <br>
        
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form enctype="multipart/form-data" class="form" action="{{ route('site.usuarios.store') }}" method="POST">
        @csrf

        <div class="form-group">
            <label for="name">Nome</label>
            <input type="text" name="name" value="{{ old('name') }}" id="name" class="form-control">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" value="{{ old('email') }}" id="email" class="form-control">
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label for="password_confirmation">Confirme a senha</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                </div>                
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="etec_id">Selecione sua Etec</label>
                    <select class="form-control" name="etec_id" id="etec_id">

                        @foreach ($etecs as $etec)
                            <option value="{{ $etec->id }}">{{ $etec->codigo . ' - ' . $etec->nome }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="col-6">
                <div class="form-group">
                    <label for="curso">Me conte sobre você na etec!</label>
                    <textarea class="form-control" name="curso" id="curso" cols="30" rows="10">{{ old('curso') }}</textarea>
                </div>                
            </div>
        </div>
        
        <div class="form-group">
            <label for="data_nascimento">Quando faz aniversário?</label>
            <input type="date" class="form-control" name="data_nascimento" value="{{ old('data_nascimento') }}" id="data_nascimento">
        </div>
        
        <div class="form-group">
            <label for="imagem_perfil">Selecione sua foto de perfil!</label>
            <input type="file" name="imagem_perfil" value="{{ old('imagem_perfil') }}" id="imagem_perfil" class="form-control">
        </div>

        <button class="btn btn-success float-right" type="submit">Cadastrar!</button>
    </form>
@endsection