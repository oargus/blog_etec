@extends('site.index')

@section('titulo', 'EtecNews')

@section('conteudo')
    
    <div class="row mt-5">
        <div class="col-8">
            {{-- BARRA DA ESQUEDA --}}
            <h1 class="text-center">Postagens</h1>
            
            @foreach ($postagens as $postagem)
                <div class="box mb-5">
                    <a href="{{ route('site.postagem', $postagem->id) }}">
                        <h1>
                            {{ $postagem->titulo }}
                            <br>
                            <span class="small text-muted">{{ $postagem->subtitulo }}</span>
                        </h1>
                    </a>

                    <img class="img img-thumbnail img-fluid" src="{{ url("storage/{$postagem->imagem}") }}" alt="{{ $postagem->titulo }}">
                    
                    
                    <p>
                        {{ Str::limit($postagem->texto, 500, '...') }}
                    </p>

                    <p>
                        Autor: {{ $postagem->user->name }} | Data: {{ $postagem->created_at->format('d/m/Y H:i:s') }} | Views: {{ $postagem->vizualizacoes }}
                    </p>

                    <a href="{{ route('site.postagem', $postagem->id) }}">Ver mais...</a>

                </div>

                <hr>



            @endforeach


        </div>

        <div class="col-4">
            {{-- BARRA DA DIREITA --}}
            <h3 class="text-center mb-3">Publicações mais famosas</h3>

            <div class="row">

                @foreach ($postagensFamosas as $postF)
                    <div class="col-12 postagem-famosa mb-2">
                        <div class="row">
                            <div class="col-4">
                                <img class="img img-thumbnail img-fluid" src="{{ url("storage/{$postF->imagem}") }}" alt="{{ $postF->titulo }}">
                            </div>
                            <div class="col-8">
                                <h4>{{ Str::limit($postF->titulo, 40, '...') }}</h4>

                                <p>
                                    {{ $postF->subtitulo }}
                                </p>
                            </div>
                        </div>

                        <hr>
                    </div>
                @endforeach


            </div>
        </div>

        <div class="col-12">
            <div class="justify-content-center d-flex">
                @if (isset($filters))
                    {{ $postagens->appends($filters)->links() }}
                @else
                    {{ $postagens->links() }}
                @endif
            </div>
        </div>
    </div>

@endsection