<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('admin.home') }}">Etec News</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbaretecnewsadmin"
        aria-controls="navbaretecnewsadmin" aria-expanded="false" aria-label="Alterna navegação">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbaretecnewsadmin">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('admin.home') }}">Inicio <span class="sr-only">(página atual)</span></a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ route('postagens.index') }}" id="postagensDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Postagens
                </a>
                <div class="dropdown-menu" aria-labelledby="postagensDropdown">
                    <a class="dropdown-item" href="{{ route('postagens.create') }}">Criar nova Postagem</a>
                    <a class="dropdown-item" href="{{ route('postagens.index') }}">Lista de Postagens</a>
                    {{-- <div class="dropdown-divider"></div> --}}
                </div>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ route('postagens.index') }}" id="postagensDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Postagens
                </a>
                <div class="dropdown-menu" aria-labelledby="postagensDropdown">
                    <a class="dropdown-item" href="{{ route('postagens.create') }}">Criar nova Postagem</a>
                    <a class="dropdown-item" href="{{ route('postagens.index') }}">Lista de Postagens</a>
                    {{-- <div class="dropdown-divider"></div> --}}
                </div>
            </li>

        {{-- <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li> --}}
        </ul>
    
        {{-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
        </form> --}}

        <form method="POST" action="{{ route('sair') }}">
            @csrf
            
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                        aria-expanded="false">{{ Auth::user()->name }}</a>

                    <div class="dropdown-menu">
                        <a href="{{ route('user.edit') }}" class="dropdown-item">Perfil</a>
                        <button type="submit" class="dropdown-item" onclick="return confirm('Deseja realmente sair do sistema?')">Sair</button>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</nav>