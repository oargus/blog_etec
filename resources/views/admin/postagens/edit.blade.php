@extends('admin.layout.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('postagens.index') }}">Postagens</a></li>
    <li class="breadcrumb-item active" aria-current="page">Editar Postagem</li>
@endsection

@section('title', "Editando - {$postagem->titulo}")

@section('conteudo')
    <h1 class="display-4">Editando - {{  $postagem->titulo }}</h1>

    @if ( $errors->any() )
        <strong>Opa!</strong> As informações inseridas não são validas <br>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    @if($mensagem = Session::get('mensagem'))
        <div class="alert alert-success">
            <p>{{ $mensagem }}</p>
        </div>
    @endif

    <form enctype="multipart/form-data" action="{{ route('postagens.update', $postagem->id) }}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="titulo">Titulo</label>
            <input value="{{ $postagem->titulo ?? old('titulo') }}" class="form-control" type="text" name="titulo" id="titulo">
        </div>

        <div class="form-group">
            <label for="subtitulo">Subtitulo</label>
            <input value="{{ $postagem->subtitulo ?? old('subtitulo') }}" class="form-control" type="text" name="subtitulo" id="subtitulo">
        </div>

        <div class="form-group">
            <label for="texto">Texto</label>
            <textarea class="form-control" name="texto" id="texto" cols="30" rows="10">{{ $postagem->texto ?? old('texto') }}</textarea>
        </div>

        <div class="form-group">
            <img class="img img-thumbnail img-fluid" src="{{ url("storage/{$postagem->imagem}") }}" alt="{{ $postagem->titulo }}">

            <label for="imagem">Selecione a imagem principal da postagem</label>
            <input type="file" class="form-control-file" id="imagem" name="imagem">
        </div>

        <button class="btn btn-warning float-right" type="submit">Atualizar Informações</button>
    </form>

@endsection