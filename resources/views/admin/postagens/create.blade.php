@extends('admin.layout.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('postagens.index') }}">Postagens</a></li>
    <li class="breadcrumb-item active" aria-current="page">Criar Postagem</li>
@endsection

@section('title', 'Nova Postagem')

@section('conteudo')
    <h1 class="display-4">Nova Postagem</h1>
    
    @if ( $errors->any() )
        <strong>Opa!</strong> As informações inseridas não são validas <br>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form enctype="multipart/form-data" action="{{ route('postagens.store') }}" method="post">
        @csrf

        <input type="hidden" name="user_id" value="1">
        
        <div class="form-group">
            <label for="titulo">Titulo</label>
            <input class="form-control" type="text" name="titulo" id="titulo" value="{{ old('titulo') }}">
        </div>

        <div class="form-group">
            <label for="subtitulo">Subtitulo</label>
            <input class="form-control" type="text" name="subtitulo" id="subtitulo" value="{{ old('subtitulo') }}">
        </div>

        <div class="form-group">
            <label for="texto">Texto</label>
            <textarea class="form-control" name="texto" id="texto" cols="30" rows="10">{{ old('texto') }}</textarea>
        </div>

        <div class="form-group">
            <label for="imagem">Selecione a imagem principal da postagem</label>
            <input type="file" class="form-control-file" id="imagem" name="imagem">
        </div>

        <button class="btn btn-primary" type="submit">Salvar</button>
    </form>

@endsection