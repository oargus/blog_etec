@extends('admin.layout.app')


@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Postagens</li>
@endsection

@section('title', 'Listagem de Postagens do Blog')

@section('conteudo')
    <h1 class="display-3">Postagens</h1>
    <a class="btn btn-success" href="{{ route('postagens.create') }}">Nova postagem</a>

    <hr>

    @if ($mensagem = Session::get('mensagem'))
        <div class="alert alert-success">
            <p>{{ $mensagem }}</p>
        </div>
    @endif

    <div class="row">
        @foreach ($postagens as $postagem)
            <div class="postagem-card col-4 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                          {{ $postagem->titulo }}
                        </h5>
                        <p class="card-subtitle">
                            {{ $postagem->subtitulo }}
                        </p>
                    </div>
                    <div class="card-footer">
                        <form onsubmit="return confirm('Deseja realmente apagar a postagem?')" action="{{ route('postagens.destroy', $postagem->id) }}" method="post">
                            @csrf
                            @method('delete')

                            <a class="btn btn-sm btn-info" href="{{ route('postagens.show', $postagem->id) }}">Ver</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('postagens.edit', $postagem->id) }}">Editar</a>

                            <button class="btn btn-sm btn-danger" type="submit">Apagar</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



@endsection