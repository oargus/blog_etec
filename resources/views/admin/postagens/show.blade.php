@extends('admin.layout.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('postagens.index') }}">Postagens</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $postagem->titulo }}</li>
@endsection

@section('title', $postagem->titulo)

@section('conteudo')

<div class="row">
    <div class="col">
        <img class="img img-thumbnail img-fluid" src="{{ url("storage/{$postagem->imagem}") }}" alt="{{ $postagem->titulo }}">
    </div>
    <div class="col">
        <h1 class="display-2 text-center">
            {{ $postagem->titulo }}
        </h1>

        <p class="text-muted small">Visualizações: {{ $postagem->vizualizacoes }}</p>
    </div>
</div>


<span class="text-muted">
    {{ $postagem->subtitulo }}
</span>

<p>
    {!! nl2br($postagem->texto) !!}
</p>

<h5>Autor: {{ $postagem->user->name }}</h5>

<hr>


<h2 class="mb-4">Comentários</h2>

<div class="comentarios">
    <div class="row">
        @foreach ($postagem->comentarios as $comentario)
            <div class="col-12 mt-3">
                <p class="imagem">
                    {{ $comentario->iniciais() }}
                </p>

                <div class="comentario">
                    <p class="texto">{{ $comentario->comentario }}</p>

                    <p class="text-muted rodape">
                        por {{ $comentario->user->name }} em {{ $comentario->created_at->format('d/m/Y H:i') }}
                    </p>
                </div>

                @if (Auth::user()->id == $comentario->user->id)
                    <form class="formulario" action="{{ route('comentarios.destroy', $comentario->id) }}" method="POST">
                        @csrf
                        @method('delete')

                        <button type="submit" onclick="return confirm('Deseja apagar o comentário?')" class="btn btn-sm btn-danger mt-1">Apagar</button>
                    </form>
                @endif
            </div>

        @endforeach
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Opa!</strong> As informações inseridas não são validas <br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($mensagem = Session::get('mensagem'))
    <div class="alert alert-success">
        <p>{{ $mensagem }}</p>
    </div>
@endif

<form method="POST" action="{{ route('comentarios.store') }}">
    @csrf

    <input type="hidden" name="commentable_id" value="{{ $postagem->id }}">
    <input type="hidden" name="commentable_type" value="App\Models\Postagem">

    <div class="form-group">
        <label for="comentario">Escreva aqui seu comentário</label>
        <textarea class="form-control" name="comentario" id="comentario" rows="3">{{ old('comentario') }}</textarea>
    </div>

    <button type="submit" class="btn btn-success ">Enviar</button>
</form>

@endsection